

name = "Miko"
age = 25
work = "web developer"
movie = "Hunger"
rating = 95.2

print(f"I am {name}, and I am {age} years old, I work as a {work}, and my rating for {movie} is {rating}%")

num1 = 2
num2 = 75
num3 = 4

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)